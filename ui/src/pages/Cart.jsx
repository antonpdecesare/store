import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import Announcement from "../components/Announcement";
import styled from "styled-components";
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import { mobile } from "../responsive";
import { useSelector } from "react-redux";
import StripeCheckout from "react-stripe-checkout";
import { useEffect, useState } from "react";
import { userRequest } from "../requestMethods"
import { useNavigate } from 'react-router-dom';

const Container = styled.div``;
const Wrapper = styled.div`
  padding: 20px;
  ${mobile({padding: "10px", })}
`;
const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;
const Top = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
`;
const Button = styled.button``;
const TopButton = styled.button`
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  border: ${(props) => props.type === "filled" && "none"};
  background-color: ${(props) =>
        props.type === "filled" ? "black" : "transparent"};
  color: ${(props) => props.type === "filled" && "white"};
`;
const TopTexts = styled.div`
${mobile({display: "none"})}
`;
const TopText = styled.span`
  text-decoration: underline;
  cursor: pointer;
  margin: 0 10px;
`;
const Bottom = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({flexDirection: "column"})}
`;
const Info = styled.div`
  flex: 3;
`;
const Summary = styled.div`
  flex: 1;
  border: 0.5px solid lightgray;
  border-radius: 10px;
  padding: 20px;
  height: 50vh;
`;
const Product = styled.div`
  display: flex;
  justify-content: space-between;
  ${mobile({flexDirection: "column"})}
`;
const ProductDetails = styled.span`
  flex: 2;
  display: flex;
`;
const Details = styled.div`
    padding: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
`;
const ProductName = styled.span``;
const ProductId = styled.span``;
const ProductColor = styled.div`
width: 20px;
height: 20px;
border: 0.5px solid black;
border-radius: 50%;
background-color: ${props=> props.color}
`;
const ProductSize = styled.span``;
const PriceDetails = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

`;
const Hr = styled.hr`
    background-color: #eee;
    border: none;
    height: 1px;

`;
const ProductAmountContainer = styled.div`
    display: flex;
    align-items: center;
`;
const ProductAmount = styled.div`
    font-size: 24px;
    margin: 5px;
    ${mobile({margin:"5px 15px"})}
`;
const ProductPrice = styled.div`
    font-size: 40px;
    font-weight: 200;
    margin-bottom: 20px;
    ${mobile({marginBottom:"20px"})}
`;
const Image = styled.img`
  width: 200px;
`;
const SummaryTitle = styled.h1`
    font-weight: 200;
`;
const SummaryItem = styled.div`
    margin: 30px 0;
    display: flex;
    justify-content: space-between;
    font-weight: ${props=>props.type === "total" && "500"};
    font-size: ${props=>props.type === "total" && "24px"};
`;
const SummaryItemText = styled.span``;
const SummaryItemPrice = styled.span``;
const CheckoutButton = styled.button`
    width: 100%;
    padding: 10px;
    background-color: black;
    color: white;
    font-weight: 600;
`;

const Cart = () => {
    const cart = useSelector(state=>state.cart)
    const [stripeToken, setStripeToken] = useState(null)
    const navigate = useNavigate()


    const onToken = (token)=>{
        setStripeToken(token);
    }


    useEffect(()=>{
        const makeRequest = async () =>{
            try {
                const res = await userRequest.post("/checkout/payment", {
                    tokenId: stripeToken.id,
                    amount: cart.total * 100,
                });
                navigate("/success",{ data: res.data });
            } catch (err) {
                console.log("I will not take your money")
            }
        }
        stripeToken && cart.total >= 1 && makeRequest();
    }, [stripeToken, cart.total, navigate])
    return (
        <Container>
            <Announcement />
            <Navbar />
            <Wrapper>
                <Title>Your Cart</Title>
                <Top>
                    <TopButton>Continue Shopping</TopButton>
                    <TopTexts>
                        <TopText>Shopping Cart(${cart.total})</TopText>
                        <TopText>Your Wishlist(0)</TopText>
                    </TopTexts>
                    <TopButton type="filled">Checkout Now!</TopButton>
                </Top>
                <Bottom>
                    <Info>
                        {cart.products.map(product=>(
                        <Product>
                            <ProductDetails>
                                    <Image src={product.img} />
                                <Details>
                                    <ProductName>
                                        <b>Item: </b> {product.title}
                                    </ProductName>
                                    <ProductId>
                                        <b>Item ID: </b> {product._id}
                                    </ProductId>
                                    <ProductColor color={product.color}>
                                    </ProductColor>
                                    <ProductSize>
                                        <b>Size: </b> {product.size}
                                    </ProductSize>
                                </Details>
                            </ProductDetails>
                            <PriceDetails>
                                <ProductAmountContainer>
                                    <RemoveIcon/>
                                    <ProductAmount>{product.quantity}</ProductAmount>
                                    <AddIcon/>
                                </ProductAmountContainer>
                                <ProductPrice>${product.price * product.quantity}</ProductPrice>
                            </PriceDetails>
                        </Product>
                        ))}
                        <Hr/>

                    </Info>
                    <Summary>
                        <SummaryTitle>
                            Order Summary
                        </SummaryTitle>
                        <SummaryItem>
                            <SummaryItemText>Subtotal : </SummaryItemText>
                            <SummaryItemPrice>${cart.total}</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Estimated Shipping Costs : </SummaryItemText>
                            <SummaryItemPrice>$5.90</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem>
                            <SummaryItemText>Shipping Discount : </SummaryItemText>
                            <SummaryItemPrice>- $5.90</SummaryItemPrice>
                        </SummaryItem>
                        <SummaryItem type="total">
                            <SummaryItemText >Total : </SummaryItemText>
                            <SummaryItemPrice>${cart.total}</SummaryItemPrice>
                        </SummaryItem>
                    <StripeCheckout
                        name="Anton's Shop"
                        image = "https://movieposters2.com/images/1632676-b.jpg"
                        billingAddress
                        shippingAddress
                        description= {`Your cart total is $${cart.total}`}
                        amount ={cart.total *100}
                        token={onToken}
                        stripeKey={"pk_test_51MtsWPDuJK0BJXP93OkK2dZHhS60RsbdFM6GNKyQTUEHhYdoqXTHrJeCPrX5QDvEUEXR0mkaCh3tUz85LEHJRRCQ00LuVLGjNx"}
                        >
                            <CheckoutButton>Checkout Now</CheckoutButton>
                    </StripeCheckout>
                    </Summary>
                </Bottom>
            </Wrapper>
            <Footer />
        </Container>
    );
};

export default Cart;
