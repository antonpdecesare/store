import styled from "styled-components"
import { popularProducts } from "../data"
import Product from "./Product";
import axios from "axios";
import { useState, useEffect } from "react";

const Container = styled.div`
display: flex;
flex-wrap: wrap;
padding: 20px:
justify-content: space-between;
`;

const renderProduct = (item, index) => <Product item={item} key={`${item.id}-${index}`} />;

const Products = ({ cat, filters, sort }) => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get(
          cat
            ? `http://localhost:8000/api/products?category=${cat}`
            : `http://localhost:8000/api/products`
        );
        setProducts(res.data)
        console.log(res)
      } catch (err) { }
    };
    getProducts()
  }, [cat])

  useEffect(() => {
    cat && setFilteredProducts(
      products.filter(item => Object.entries(filters).every(([key, value]) =>
        item[key].includes(value)
      )
      )
    );
  }, [products, cat, filters]);

  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts(prev =>
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      );
    } else if (sort === "asc") {
      setFilteredProducts(prev =>
        [...prev].sort((a, b) => a.price - b.price)
      );
    } else {
      setFilteredProducts(prev =>
        [...prev].sort((a, b) => b.price - a.price)
      );
    }
  }, [sort])


  return (
    <Container>
      {cat
        ? filteredProducts.map(renderProduct)
        : products.map(renderProduct)
      }
    </Container>
  );
};

export default Products
