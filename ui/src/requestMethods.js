import axios from "axios";

const BASE_URL = "http://localhost:8000/api/"
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0MmRjM2M3MTA5OGQyMmFiYTdlNDIyMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY4MTUxMDA0MywiZXhwIjoxNjgxNzY5MjQzfQ.vdip7qBo398L5ZQg9GWL7fnE5mQeys3rFeV_yD0-GUc"

export const publicRequest = axios.create({
    baseURL: BASE_URL,
});


export const userRequest = axios.create({
    baseURL: BASE_URL,
    headers: {token: `Bearer ${TOKEN}`}
});
