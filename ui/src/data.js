export const sliderItems = [
    {
        id: 1,
        img: "https://i.pinimg.com/564x/65/4f/30/654f30a8458a8f6eaebf60567fd80940.jpg",
        title: "SUMMER SALE",
        desc: "Get ready to update your wardrobe for the season with our summer clothing sale! We've got the hottest styles at unbeatable prices, so you can look and feel your best without breaking the bank.",
        bg: "f5fafd",
    },
    {
        id: 2,
        img: "https://i.pinimg.com/564x/fe/33/05/fe330595615d22af6995b920f97da28c.jpg",
        title: "AUTUMN COLLECTION",
        desc: "As the leaves start to turn and the air gets crisper, it's time to refresh your wardrobe for fall! Our latest collection of clothes has everything you need to stay stylish and comfortable during the season.",
        bg: "fcf1ed",

    },
    {
        id: 3,
        img: "https://i.pinimg.com/564x/5e/57/cd/5e57cde3ccc548a9db7d480f87fd6abd.jpg",
        title: "LOUNGEWEAR LOVE",
        desc: "When it comes to lounging, comfort is key - and that's exactly what our lounge wear collection delivers! Say goodbye to stiff fabrics and uncomfortable fits and hello to cozy, soft, and stylish pieces that you'll never want to take off.",
        bg: "fbf0f4",
    },
];

export const categories = [
    {
        id: 1,
        img: "https://images.pexels.com/photos/5886041/pexels-photo-5886041.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "SHIRT STYLE!",
        cat: "womens",
    },
    {
        id: 2,
        img: "https://images.pexels.com/photos/2983464/pexels-photo-2983464.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        title: "LOUNGEWEAR LOVE",
        cat: "comfy",
    },
    {
        id: 3,
        img: "https://images.pexels.com/photos/5480696/pexels-photo-5480696.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
        title: "LIGHT JACKETS",
        cat: "coat",
    },
];

export const popularProducts = [
    {
        id: 1,
        img: "https://d3o2e4jr3mxnm3.cloudfront.net/Mens-Jake-Guitar-Vintage-Crusher-Tee_68382_1_lg.png",
    },
    {
        id: 2,
        img: "https://cottonbureau.imgix.net/products/originals/43011_jwQT.png?ixlib=vue-2.9.0&auto=compress%2Cformat&fit=max&w=1678",
    },
    {
        id: 3,
        img: "https://www.prada.com/content/dam/pradanux_products/U/UCS/UCS319/1YOTF010O/UCS319_1YOT_F010O_S_182_SLF.png",
    },
    {
        id: 4,
        img: "https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T2/images/I/71JOvu3-jLL._AC_UL800_FMwebp_QL65_.jpg",
    },
    {
        id: 5,
        img: "https://images.ctfassets.net/5gvckmvm9289/3BlDoZxSSjqAvv1jBJP7TH/65f9a95484117730ace42abf64e89572/Noissue-x-Creatsy-Tote-Bag-Mockup-Bundle-_4_-2.png",
    },
    {
        id: 6,
        img: "https://d3o2e4jr3mxnm3.cloudfront.net/Rocket-Vintage-Chill-Cap_66374_1_lg.png",
    },
    {
        id: 7,
        img: "https://benscore.com/media/catalog/product/cache/abc0b8b21c324bfe3f686b3f59715c9e/2/2/2217_arrow_jacket_forest.png",
    },
    {
        id: 8,
        img: "https://www.pngarts.com/files/3/Women-Jacket-PNG-High-Quality-Image.png",
    },
]
